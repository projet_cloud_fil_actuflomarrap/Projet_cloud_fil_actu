import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ChangerMdpComponent } from './changer-mdp/changer-mdp.component';
import { PostComponent } from './post/post.component';
import { FilComponent } from './fil/fil.component';
import { CreerCompteComponent } from './creer-compte/creer-compte.component';
import { AjouterAmiComponent } from './ajouter-ami/ajouter-ami.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChangerMdpComponent,
    PostComponent,
    FilComponent,
    CreerCompteComponent,
    AjouterAmiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
