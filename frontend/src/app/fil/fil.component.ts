import { Component, OnInit } from '@angular/core';
import { MessageService, PhpData } from '../message/message.service';

@Component({
  selector: 'app-fil',
  templateUrl: './fil.component.html',
  styleUrls: ['./fil.component.scss']
})
export class FilComponent implements OnInit {
  posts! : Array<any>
  amis! : Array<any>

  constructor(private ms : MessageService) {
    
  }

  ngOnInit(): void {
    this.posts = [];
    this.ms.sendMessage("AfficherPosts", {}).subscribe((msg : PhpData) =>
      {
        if(msg.status == "ok")
          this.posts = msg.data;
      }
    );
    
    this.amis = [];
    this.ms.sendMessage("AfficherAmis", {}).subscribe((msg : PhpData) =>
        {
          if(msg.status == "ok")
            this.amis = msg.data;
        }
    );
  }

  ngAfterViewInit(): void {
    
  }

}
