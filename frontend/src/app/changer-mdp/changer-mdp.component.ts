import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, PhpData } from '../message/message.service';

@Component({
  selector: 'app-changer-mdp',
  templateUrl: './changer-mdp.component.html',
  styleUrls: ['./changer-mdp.component.scss']
})
export class ChangerMdpComponent implements OnInit {

  pseudo = '';
  mdp = '';
  mdp_conf = '';
  error_msg = '';

  constructor(private ms : MessageService, private router:Router) { }

  ngOnInit(): void {
  }

  check() {
    if(this.mdp == this.mdp_conf)
    {
      this.error_msg = "";
      this.ms.sendMessage('MailUtilisateur', {pseudo: this.pseudo, mdp: this.mdp}).subscribe((msg : PhpData) => {
        if(msg.status == "error")
        {
          this.error_msg = msg.data.reason;
        }
        else
        {
          this.router.navigateByUrl('/fil');
        }
      });
    }
    else
    {
      this.error_msg = "Le mot de passe ne correspond pas."
    }
  }

}
