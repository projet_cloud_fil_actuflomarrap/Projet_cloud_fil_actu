import { Component, OnInit } from '@angular/core';
import { PhpData, MessageService } from '../message/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-creer-compte',
  templateUrl: './creer-compte.component.html',
  styleUrls: ['./creer-compte.component.scss']
})
export class CreerCompteComponent implements OnInit {

  pseudo = '';
  mdp = '';
  mdp_conf = '';
  error_msg = '';
  email = '';

  constructor(private ms : MessageService, private router: Router) {
  }

  ngOnInit(): void {
  }

  check() {
    if(this.mdp == this.mdp_conf)
    {
      this.error_msg = "";
      this.ms.sendMessage('AjouterUtilisateur', {pseudo: this.pseudo, mdp: this.mdp, email: this.email}).subscribe((msg : PhpData) => {
        if(msg.status == "error")
        {
          this.error_msg = msg.data.reason;
        }
        else
        {
          this.router.navigateByUrl('/login');
        }
      });
    }
    else
    {
      this.error_msg = "Le mot de passe ne correspond pas."
    }
  }

}
