import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterAmiComponent } from './ajouter-ami.component';

describe('AjouterAmiComponent', () => {
  let component: AjouterAmiComponent;
  let fixture: ComponentFixture<AjouterAmiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjouterAmiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterAmiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
