import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, PhpData } from '../message/message.service';

@Component({
  selector: 'app-ajouter-ami',
  templateUrl: './ajouter-ami.component.html',
  styleUrls: ['./ajouter-ami.component.scss']
})
export class AjouterAmiComponent implements OnInit {
  constructor(private ms : MessageService, private router : Router) { }
  users! : Array<any>
  ngOnInit(): void {

    this.users = [];
    this.ms.sendMessage("AfficherUtilisateurs", {}).subscribe((msg : PhpData) =>
        {
          if(msg.status == "ok")
          console.log(msg.data);
            this.users = msg.data;
        }
    );
  }

  send(pseudo : string) {
    this.ms.sendMessage('AjouterAmi', {pseudo: pseudo}).subscribe((msg : PhpData) => {
        if(msg.status == "ok")
        {
          this.router.navigateByUrl('/fil');
        }
    });
  }


}
