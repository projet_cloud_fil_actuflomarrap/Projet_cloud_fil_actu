import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PostComponent } from './post/post.component';
import { FilComponent } from './fil/fil.component';
import { ChangerMdpComponent } from './changer-mdp/changer-mdp.component';
import { CreerCompteComponent } from './creer-compte/creer-compte.component';
import { AjouterAmiComponent } from './ajouter-ami/ajouter-ami.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'post', component: PostComponent},
  {path: 'fil', component: FilComponent},
  {path: 'changer-mdp', component: ChangerMdpComponent},
  {path: 'creer-compte', component: CreerCompteComponent},
  {path: 'ajouter-ami', component: AjouterAmiComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
