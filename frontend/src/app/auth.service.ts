import { Injectable } from '@angular/core';
import { PhpData, MessageService } from './message/message.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public static connected = false

  constructor(private ms : MessageService) { }

  sendAuthentication(login: string, password : string) : Observable<PhpData> {
    return this.ms.sendMessage('checkLogin', {pseudo: login, password: password});
  }

  finalizeAuthentication(msg : PhpData) : boolean {
    AuthService.connected = msg.status=="ok";
    return AuthService.connected;
  }
}

