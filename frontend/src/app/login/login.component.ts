import { HttpClient, HttpHandler } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message/message.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  error_msg = '';
  pseudo = '';
  mdp = '';
  constructor(private as : AuthService, private router: Router) {}

  ngOnInit(): void {
    
  }
  
  getError() : string {
	return this.error_msg;
  }
  
  setError(msg : string): void {
	this.error_msg = msg;
  }

  check() : void {
    this.as.sendAuthentication(this.pseudo, this.mdp).subscribe(msg => {
      if(this.as.finalizeAuthentication(msg))
        this.router.navigateByUrl('/fil');
      else
          this.error_msg = msg.data.reason;
    });
  }
}
