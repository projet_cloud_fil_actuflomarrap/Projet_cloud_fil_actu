import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, PhpData } from '../message/message.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  titre = ''
  contenu = ''
  error_msg = ''

  constructor(private ms : MessageService, private router:Router) { }

  ngOnInit(): void {
  }

  send() {
      this.ms.sendMessage('AjouterPost', {titre: this.titre, contenu: this.contenu}).subscribe((msg : PhpData) => {
        if(msg.status == "error")
        {
          this.error_msg = msg.data.reason;
        }
        else
        {
          this.router.navigateByUrl('/fil');
        }
      });
  }

}
