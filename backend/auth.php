<?php
require_once("helper.php");
require_once('mysql/mysqlConnect.php');

function printBool($bool_param) {
  if($bool_param)
    print "true";
  else {
    print "false";
  }
}

function printLineBool($bool_param) {
  if($bool_param)
    print "true<br>";
  else {
    print "false<br>";
  }
}

ini_set('session.cookie_samesite', 'None');
session_start();
#session_start(['cookie_samesite' => 'Lax']);
#$_SESSION += ['test' => "test des sessions"];
#print_r($_SESSION);
function isAuthenticated() {
    return array_key_exists('pseudo', $_SESSION);
}

function authenticate() {
  global $PDO;
    $flag = false;
    if(array_key_exists('pseudo', $_REQUEST) && array_key_exists('password', $_REQUEST))
    {
        $query = "SELECT * FROM User WHERE pseudo = ?";
        $data = array($_REQUEST["pseudo"]);
        $statement = $PDO->prepare( $query );
        $exec = $statement->execute( $data );
        $resultats = $statement->fetchAll ( PDO::FETCH_ASSOC );
        $tab = array();
        foreach($resultats as $un_resultat)
        {
            $tab[] = $un_resultat;
        }
        if(count($tab) > 0)
        {
            $_SESSION["ID"] = $tab[0]["ID"];
            $_SESSION["pseudo"] = $_REQUEST["pseudo"];
            $flag = true;
        }
    }
    return $flag;
}
?>
