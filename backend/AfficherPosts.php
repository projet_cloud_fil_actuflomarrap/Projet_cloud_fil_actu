<?php
    //Ici on s'occupe de l'affichage des posts

    //On récupère mysqlConnect
    require_once("mysql/mysqlConnect.php");
    require_once ('helper.php');

    $query = "SELECT DISTINCT Post.*, User.pseudo FROM Post, Abonnement, User WHERE ((Post.ID_User = Abonnement.ID_Suivi AND
    Abonnement.ID_Suiveur = ?) OR Post.ID_User = ?) AND Post.ID_User = User.ID
    ORDER BY Post.estampille DESC";
    $data = array($_SESSION["ID"], $_SESSION["ID"]);
    $statement = $PDO->prepare($query);
    $exec = $statement->execute( $data );
    $resultats = $statement->fetchAll ( PDO::FETCH_ASSOC );
    $tab = array();
    foreach($resultats as $un_resultat)
    {
        $tab[] = $un_resultat;
    }

    sendMessage($tab);
?>