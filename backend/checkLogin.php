<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    require_once("helper.php");
    require_once("auth.php");

    if(authenticate()) {
        sendMessage("Authentification réussie");
    }
    else {
        sendError("Le login et le mot de passe ne correspondent pas");
    }

?>