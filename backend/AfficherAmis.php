<?php
    require_once("mysql/mysqlConnect.php");
    require_once("helper.php");


    $query = "SELECT pseudo FROM User 
    INNER JOIN Abonnement ON User.ID = Abonnement.ID_Suivi
    WHERE Abonnement.ID_Suiveur = ?";
    $data = array($_SESSION["ID"]);
    $statement = $PDO->prepare($query);
    $exec = $statement->execute( $data );
    $resultats = $statement->fetchAll ( PDO::FETCH_ASSOC );
    $tab = array();
    foreach($resultats as $un_resultat)
    {
        $tab[] = $un_resultat;
    }
    sendMessage($tab);
?>