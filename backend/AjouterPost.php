<?php
    //On s'intéresse à l'ajout d'un post
    require_once ('mysql/mysqlConnect.php');
    require_once ('helper.php');

    $query = " INSERT INTO Post (titre, contenu, ID_User)
    VALUES (?, ?, ?)";
    $data = array($_POST["titre"], $_POST["contenu"], $_SESSION["ID"]);
    $statement = $PDO->prepare( $query );
    $exec = $statement->execute( $data );
    $resultats = $statement->fetchAll ( PDO::FETCH_ASSOC );
    $tab = array();
    foreach($resultats as $un_resultat)
    {
        $tab[] = $un_resultat;
    }
    sendMessage("OK");
?>