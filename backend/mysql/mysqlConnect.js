const config = require('../config');
const mysql = require('mysql2');

const db = mysql.createConnection({
    host: config.mysqlHost, 
    user: config.mysqlLogin,
    password: config.mysqlPassword,
    database: config.mysqlDatabase
});

db.connect();

module.exports = db;