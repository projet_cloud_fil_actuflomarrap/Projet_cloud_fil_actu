const config = require('../config');
const db = require ('./mysqlConnect');

function getPosts (idUser) {
    const query = `
        SELECT * FROM ${config.Post} 
        INNER JOIN ${config.Abonnement} ON Post.ID_User = Abonnement.ID_Suivi 
        WHERE Abonnement.ID_Suiveur = ? 
        ORDER BY Post.Estampille DESC`;          //Récupérer les posts dont les ID_User sont correspondants dans la table Abonnement à l'ID entré
    const data = [idUser];

    return new Promise((resolve, reject) => {
        db.query(query, data, (err, rows) => {
            if(err) return reject(err);
            resolve(rows);
        });
    });
}
module.exports.getPosts = getPosts;

function addPost (titre, contenu, idUser) {
    const query = `
        INSERT INTO ${config.Post} (Titre, Contenu, ID_User)
        VALUES (?, ?, ?)`;
    const data = [titre, contenu, idUser];

    return new Promise((resolve, reject) => {
        db.query(query, data, (err, rows) => {
            if(err) return reject(err);
            resolve(rows);
        });
    });

}
module.exports.addPost = addPost;

function creerCompte (password, pseudo, email) {
    const query = `
        INSERT INTO ${config.User} (Password, Pseudo, Email)
        VALUES (?, ?, ?)`;
    const data = [password, pseudo, email];

    return new Promise((resolve, reject) => {
        db.query(query, data, (err, rows) => {
            if(err) return reject(err);
            resolve(rows);
        });
    });

}
module.exports.creerCompte = creerCompte;

function checkLogin (login) {
    const query = `
        SELECT password FROM ${config.User} WHERE ID = ?`;
    const data = [login];

    return new Promise((resolve, reject) => {
        db.query(query, data, (err, rows) => {
            if(err) return reject(err);
            resolve(rows);
        });
    });
}
module.exports.checkLogin = checkLogin;

function getIdPseudo (pseudo) {
    const query = `
        SELECT ID FROM ${config.User} WHERE pseudo = ?`;
    const data = [pseudo];

    return new Promise((resolve, reject) => {
        db.query(query, data, (err, rows) => {
            if(err) return reject(err);
            resolve(rows);
        });
    });
}
module.exports.getIdPseudo = getIdPseudo;

function ajoutAmi (ID_follow,login) {
    const query = `
        INSERT INTO ${config.Abonnement} (ID_Suiveur, ID_Suivi)
        VALUES (?, ?)`;
    const data = [login,ID_follow];

    return new Promise((resolve, reject) => {
        db.query(query, data, (err, rows) => {
            if(err) return reject(err);
            resolve(rows);
        });
    });
}
module.exports.ajoutAmi = ajoutAmi;

function getFollow (login) {
    const query = `
        SELECT Pseudo FROM ${config.User} 
        INNER JOIN ${config.Abonnement} ON User.ID = Abonnement.ID_Suivi
        WHERE Abonnement.ID_Suiveur = ?`;
    const data = [login];

    return new Promise((resolve, reject) => {
        db.query(query, data, (err, rows) => {
            if(err) return reject(err);
            resolve(rows);
        });
    });
}
module.exports.getFollow = getFollow;