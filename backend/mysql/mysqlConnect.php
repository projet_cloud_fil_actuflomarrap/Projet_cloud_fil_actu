<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    require_once('config.php');
    $opt = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,PDO::ATTR_EMULATE_PREPARES =>false);
    $PDO = new PDO($dsn, $mysqlLogin, $mysqlPassword, $opt);
?>