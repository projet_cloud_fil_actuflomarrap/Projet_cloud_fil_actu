<?php
    //Cette partie concerne l'ajout d'un utilisateur en follower
    require_once ('mysql/mysqlConnect.php');
    require_once ('helper.php');

    $query = " SELECT ID FROM User WHERE pseudo = ?";
    $data = array($_REQUEST["pseudo"]);
    $statement = $PDO->prepare( $query );
    $exec = $statement->execute( $data );
    $resultats = $statement->fetchAll ( PDO::FETCH_ASSOC );
    $tab1 = array();
    foreach($resultats as $un_resultat)
    {
        $tab1[] = $un_resultat;
    }

    $query = " SELECT ID FROM User WHERE pseudo = ?";
    $data = array($_SESSION["pseudo"]);
    $statement = $PDO->prepare( $query );
    $exec = $statement->execute( $data );
    $resultats = $statement->fetchAll ( PDO::FETCH_ASSOC );
    $tab2 = array();
    foreach($resultats as $un_resultat)
    {
        $tab2[] = $un_resultat;
    }

    $query2 = "INSERT INTO Abonnement (ID_Suiveur, ID_Suivi)
    VALUES (?, ?)";
    $data2 = array($tab2[0]["ID"],$tab1[0]["ID"]);
    $statement2 = $PDO->prepare( $query2 );
    $exec2 = $statement2->execute( $data2 );
    $resultats2 = $statement2->fetchAll ( PDO::FETCH_ASSOC );
    $tab2 = array();
    foreach($resultats2 as $un_resultat2)
    {
        $tab2[] = $un_resultat2;
    }

    sendMessage($tab2);

?>