CREATE DATABASE c17016430;
CREATE USER flo;
GRANT ALL PRIVILEGES ON *.* TO flo;
\u c17016430
CREATE TABLE User (ID SMALLINT PRIMARY KEY NOT NULL AUTO_INCREMENT, pseudo VARCHAR(50),  password VARCHAR(50), email VARCHAR(30));
CREATE TABLE Abonnement (ID_Suiveur SMALLINT NOT NULL, ID_Suivi SMALLINT NOT NULL);
CREATE TABLE Post (ID SMALLINT NOT NULL PRIMARY KEY AUTO_INCREMENT, titre VARCHAR(20) NOT NULL, contenu VARCHAR(248) NOT NULL, estampille TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, ID_User SMALLINT NOT NULL);